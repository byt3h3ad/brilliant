# Brilliant Archive

First of all, a big thank you to everyone on Brilliant Community for making it an awesome place.

I am aiming to make this repository into an archive of Brilliant's community with everyone's content accessible at one place. 

### How do I add my content?

It'll be indexed alphabetically. Just upload your zip in the required directory.

### Why Gitlab?

Gitlab does not have file and repository size restrictions. Also supports $`\LaTeX`$

### Future plans

Honestly I don't know if I'll ever use Brilliant again. With no viable alternatives (for STEM), it is indeed a huge blow for us all. The best we can do archive our content and hope we can find a new home.

### Contributions

- First and foremost, please upload your content and spread the word.

- Any ideas to improve this humble effort will be much appreciated.

### To-do

- [ ] ability to be able to unzip the folder and access the contents on Gitlab itself. 
